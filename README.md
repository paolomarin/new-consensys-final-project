# Consensys Academy Final Project

The aim of this project was to learn how to build a simple decentralised application that interacts with the Ethereum Rinkeby testnet using truffle development and testing framework. 


## User Stories  

The user enters text into a web form and send the transaction then uses Metamask to sign the transaction to the Ethereum Rinkeby blockchain. When the transaction is completed, the web app will output information underneath the web form to acknowledge the user that the text was successfully submitted to the blockchain and IPFS.


## Running the app successfully

   Clone the repository to your local computer and follow the installation instructions below.

## Installation

1. Install Truffle and Ganache globally.
    ```javascript
    $ npm install -g truffle
    $ npm install -g ganache-cli
    ```

2. Clone the repository, cd into the directory, and install node modules.
    ```javascript
    git clone https://gitlab.com/paolomarin/new-consensys-final-project.git
    $ cd new-consensys-final-project
    $ npm install
    ```

3. Open another terminal and run the Ganache test blockchain.new-
    ```javascript
    $ ganache-cli
    ```

4. Compile and migrate the smart contracts.
    ```javascript
    $ truffle compile
    $ truffle migrate
    ```

5. Run the tests to make sure the contract is working correctly.
    ```javascript
    $ truffle test
    ```

6. Serve a local instance of the application on your machine.
    ```javascript
    $ npm start
    The server will launch the app at http://localhost:3000
    ```

7. Open Metamatask and select the localhost on port 8545 for signing the transaction.
   Please note the deployed_address.txt file for the Rinkeby Test Network contract address.
   Enjoy the unstoppable Ethereum!!
