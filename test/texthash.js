var TextHash = artifacts.require('./TextHash.sol');

contract('TextHash', (accounts) => {
  const [firstAccount, secondAccount] = accounts;

  it("Sets an owner of the contract", async () => {
    const texthash = await TextHash.new();
    assert.equal(await texthash.owner.call(), firstAccount);
  });

  it("Checks if copyright is false", async () => {
    const texthash = await TextHash.new();
    assert.equal(await texthash.checkText.call("message"), false);
  });

  it("Checks if copyright is true", async () => {
    const texthash = await TextHash.new();
    texthash.saveText("message");
    assert.equal(await texthash.checkText.call("message"), true);
  });

  it("Another account cannot access contract", async () => {
    const texthash = await TextHash.new();
    let newAcccount = await texthash.checkText("message", {from: secondAccount});
    assert.equal(newAcccount, false);
  });

  it("Transfers contract ownership", async () => {
    const texthash = await TextHash.new();
    texthash.transferOwnership(secondAccount);
    assert.equal(await texthash.owner.call(), secondAccount);
  });
});
