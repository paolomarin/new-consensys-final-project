# Final Project Security Implementations

The general design of the TextHash Solidity contract was to keep its logic and security as balance and simple as possible. In programming smart contract the cost of failure can be high and changes to the code can be difficult. Following Ethereum smart contract security best practices, the smart contract code was tested for logic bugs, integer arithmetic overflow and exposed functions in the following ways:

## Logic Bugs

  * Maintained the contract logic as simple as possible and introduced modular code to keep contracts and functions small.
  * Running unit tests to ensure code quality and mitigate against security vulnerabilities.


## Integer Arithmetic Overflow

  * In computer programming, an integer overflow/underflow occurs when an arithmetic operation attempts to create a numeric value that is  outside of the range that can be represented with a given number of bits – either larger than the maximum or lower than the  minimum representable value. For example an overflow happens when an arithmetic operation reach the maximum size of the type: that's it, if you store your number in the uint256  type, it means your number will be stored in a 256 bits unsigned number ranging from 0 to 2^256.
  * Implemented the OpenZeppelin security library `SafeMath` to prevent overflows when working with `uint`.


## Exposed Functions

  * Audited all functions to determine which should be accessible to anybody working with the contract and which should only be accessible only to the contract owner.
  * The `_transferOwnership(address)` function was found to only be needed by the owner of the contract.
