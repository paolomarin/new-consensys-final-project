pragma solidity ^0.4.24;

// ----------------------------------------------------------------------------
// ConsenSys Academy’s 2018 Developer Program Final Project
//
// https://gitlab.com/paolomarin/consensys-project
//
// Enjoy! (c) paolomarin / The MIT Licence.
// ----------------------------------------------------------------------------

import 'openzeppelin-solidity/contracts/ownership/Ownable.sol';
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

// Contract title TextHash
contract TextHash is Ownable {

 // SafeMath used to prevent overflow when working with bytes
 using SafeMath for bytes32;

 // mapping bytes to bool and assigned to private variable proofs
 mapping (bytes32 => bool) private proofs;

 // checks a bytes32 array and assigns the boolean true
 function storeProof(bytes32 proof) private {
		proofs[proof] = true;
	}

	// checks a bytes32 array for boolean value and return proofs[proof] as boolean true or false
	function hasProof(bytes32 proof) private view returns (bool) {
		return proofs[proof];
	}

	// turns a string into sha256 encoded byte array and return sha256 encoded byte array
  function hashText(string text) private pure returns (bytes32) {
		return sha256(abi.encodePacked(text));
	}

  // only the Owner of the contract can save text into hash
	function saveText(string text) public {
		bytes32 proof = hashText(text);
		storeProof(proof);
	}

  // checks the text of a string and if it has been hashes returns boolean
  function checkText(string text) public view returns (bool) {
		bytes32 proof = hashText(text);
		return hasProof(proof);
	}

  // transfer contract to another owner
	function _transferOwnership(address _newOwner) internal {
        require(_newOwner != address(0));
        emit OwnershipTransferred(owner, _newOwner);
        owner = _newOwner;
    }

  // if anyone calls this contract then the contract will self destruct
	function kill() public {
    	if (msg.sender == owner) {
	     selfdestruct(owner);
	}
    }
}
