# Contract Design

The contract design patterns decisions have been driven mainly for didactic purpose. Simplicity was important as to better understand how the various interactions works in building and end to end unstoppable Ethereum application.

The TextHash Solidity contract try to address the following question:
How can you proof the ownership of a text message that has been sent on the Ethereum Blockchain?
The solution to this problem is to submit a true or false result to proof if the text was submitted to the Ethereum blockchain and store the hash of the text in the Inter Planetary File System.

## High Level Overview

The purpose of the `TexhHash Contract` is to `verify` that some text has been sent to the Ethereum Blockchain through the return of a transaction number and a proof of an hash stored on IPFS. To ensure code quality and mitigate against security vulnerabilities both the `SafeMath` and `Ownable` libraries were imported using [OpenZeppelin](https://openzeppelin.org/). This contract has been deployed to the Rinkeby testnet. The address is: 0x04503b532fA8015770a05533Cf50266eC1fff32A

## Contract Data Structure

* ***saveText(string)*** - Takes a string, hashes it using the hashText function, and stores the output (`proof`) in the storeProof function.

* ***hashText(proof)*** - Hashes a string using the sha256 algorithm.

* ***storeProof(proof)*** - Stores and returns a boolean output.

* ***hasProof(proof)*** - Inputs a byte array and outputs a boolean value.

* ***checkText(string)*** - Takes a string, hashes it using the hashText function, and outputs the storeProof function.

* ***transferOwnership(address)*** - Takes a new address and transfers ownership of the contract to that address.

* ***kill()*** - Self destructs the contract if something goes wrong and acts as a kill safety switch.

```
function saveText(string text) public {
  bytes32 proof = hashText(text);
  storeProof(proof);
}


function hashText(string text) private pure returns (bytes32) {
  return sha256(abi.encodePacked(text));
}


  function storeProof(bytes32 proof) private {
  proofs[proof] = true;
}


function hasProof(bytes32 proof) private view returns (bool) {
  return proofs[proof];
}


function checkText(string text) public view returns (bool) {
  bytes32 proof = hashText(text);
  return hasProof(proof);
}


function _transferOwnership(address _newOwner) internal {
    require(_newOwner != address(0));
    emit OwnershipTransferred(owner, _newOwner);
    owner = _newOwner;
}


  function killContract() public {
  selfdestruct(owner);
}
```
